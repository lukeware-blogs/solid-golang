package pedido

import (
	"labsit.io/solid-golang/entities/cliente"
	"labsit.io/solid-golang/entities/endereco"
	"labsit.io/solid-golang/entities/produto"
)

// Utilizando design pattern builder, Utilizando DIP
type carrinhoCompraBuilder struct {
	cliente         cliente.ICliente
	itens           []produto.IItem
	enderecoEntrega endereco.IEndereco
}

func New() *carrinhoCompraBuilder {
	return new(carrinhoCompraBuilder)
}

func (c *carrinhoCompraBuilder) Cliente(cliente cliente.ICliente) *carrinhoCompraBuilder {
	c.cliente = cliente
	return c
}

func (c *carrinhoCompraBuilder) Itens(itens []produto.IItem) *carrinhoCompraBuilder {
	c.itens = itens
	return c
}

func (c *carrinhoCompraBuilder) EnderecoEntrega(enderecoEntrega endereco.IEndereco) *carrinhoCompraBuilder {
	c.enderecoEntrega = enderecoEntrega
	return c
}

func (c *carrinhoCompraBuilder) Build() ICarrinhoCompra {
	return &carrinhoCompra{
		pedido: pedido{
			cliente:         c.cliente,
			itens:           c.itens,
			enderecoEntrega: c.enderecoEntrega,
		},
		status: "ABERTO",
	}
}
