package pedido

import (
	"labsit.io/solid-golang/entities/cliente"
	"labsit.io/solid-golang/entities/endereco"
	"labsit.io/solid-golang/entities/produto"
)

// Utilizando ISP
type IPedido interface {
	Cliente() cliente.ICliente
	Itens() []produto.IItem
	EnderecoEntrega() endereco.IEndereco
	Total() float64
	TotalFrete() float64
	ValidarDados() (bool, error)
}
