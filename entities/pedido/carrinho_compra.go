package pedido

// Utilizando OCP e LSP
type carrinhoCompra struct {
	pedido
	status string // Aberto, Finalizado ou Abandomado
}

func (c carrinhoCompra) Status() string {
	return c.status
}

func (c *carrinhoCompra) SetStatus(status string) {
	c.status = status
}
