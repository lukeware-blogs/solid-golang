package pedido

// Utilizando ISP
type ICarrinhoCompra interface {
	IPedido
	Status() string
	SetStatus(status string)
}
