package pedido

import (
	"fmt"

	"labsit.io/solid-golang/entities/cliente"
	"labsit.io/solid-golang/entities/endereco"
	"labsit.io/solid-golang/entities/produto"
)

// Utilizando OCP
type pedido struct {
	cliente         cliente.ICliente
	itens           []produto.IItem
	enderecoEntrega endereco.IEndereco
}

func (p pedido) Cliente() cliente.ICliente {
	return p.cliente
}

func (p pedido) Itens() []produto.IItem {
	return p.itens
}

func (p pedido) EnderecoEntrega() endereco.IEndereco {
	return p.enderecoEntrega
}

func (p pedido) Total() float64 {
	total := 0.0
	for _, item := range p.itens {
		total = total + (item.PrecoComDesconto() * float64(item.Quantidade()))
	}
	return total
}

func (p pedido) TotalFrete() float64 {
	total := 0.0
	for _, item := range p.itens {
		total = total + item.Frete()
	}
	return total
}

func (p pedido) ValidarDados() (bool, error) {
	switch {
	case p.cliente == nil:
		return false, fmt.Errorf("Cliente não encontrado")
	case p.itens == nil || len(p.itens) == 0:
		return false, fmt.Errorf("Itens não encontrado")
	case p.enderecoEntrega == nil:
		return false, fmt.Errorf("Endereço não encontrado")
	}

	clienteValido, err := p.cliente.ValidarDados()
	if !clienteValido {
		return false, err
	}

	enderecoValido, err := p.enderecoEntrega.ValidarDados()
	if !enderecoValido {
		return false, err
	}

	for _, item := range p.itens {
		itemValido, err := item.ValidarItens()
		if !itemValido {
			return false, err
		}
	}
	return true, nil
}
