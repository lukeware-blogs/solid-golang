package imposto

// Utilizando design pattern builder, Utilizando DIP
type impostoBuilder struct {
	icms   float64
	pis    float64
	cofins float64
}

func New() *impostoBuilder {
	return new(impostoBuilder)
}

func (i *impostoBuilder) Icms(icms float64) *impostoBuilder {
	i.icms = icms
	return i
}

func (i *impostoBuilder) Pis(pis float64) *impostoBuilder {
	i.pis = pis
	return i
}

func (i *impostoBuilder) Cofins(cofins float64) *impostoBuilder {
	i.cofins = cofins
	return i
}

func (i *impostoBuilder) Build() IImposto {
	return imposto{
		icms:   i.icms,
		pis:    i.pis,
		cofins: i.cofins,
	}
}
