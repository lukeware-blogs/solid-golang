package imposto

// Utilizando OCP
type imposto struct {
	icms   float64
	pis    float64
	cofins float64
}

func (i imposto) Icms() float64 {
	return i.icms
}

func (i imposto) Pis() float64 {
	return i.pis
}

func (i imposto) Cofins() float64 {
	return i.cofins
}
