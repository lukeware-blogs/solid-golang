package imposto

// Utilizando ISP
type IImposto interface {
	Icms() float64
	Pis() float64
	Cofins() float64
}
