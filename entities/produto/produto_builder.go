package produto

import (
	"labsit.io/solid-golang/entities/estoque"
	"labsit.io/solid-golang/entities/imposto"
)

// Utilizando design pattern builder, Utilizando DIP
type ProdutoBuilder struct {
	descricao          string
	preco              float64
	frete              float64
	percentualDesconto float64
	quantidade         uint64
	estoque            estoque.IEstoque
	imposto            imposto.IImposto
}

func New() *ProdutoBuilder {
	return new(ProdutoBuilder)
}

func (i *ProdutoBuilder) Descricao(descricao string) *ProdutoBuilder {
	i.descricao = descricao
	return i
}

func (i *ProdutoBuilder) Preco(preco float64) *ProdutoBuilder {
	i.preco = preco
	return i
}

func (i *ProdutoBuilder) Frete(frete float64) *ProdutoBuilder {
	i.frete = frete
	return i
}

func (i *ProdutoBuilder) PercentualDesconto(percentualDesconto float64) *ProdutoBuilder {
	i.percentualDesconto = percentualDesconto
	return i
}

func (i *ProdutoBuilder) Quantidade(quantidade uint64) *ProdutoBuilder {
	i.quantidade = quantidade
	return i
}

func (i *ProdutoBuilder) Estoque(estoque estoque.IEstoque) *ProdutoBuilder {
	i.estoque = estoque
	return i
}

func (i *ProdutoBuilder) Imposto(imposto imposto.IImposto) *ProdutoBuilder {
	i.imposto = imposto
	return i
}

func (p ProdutoBuilder) Build() IItem {
	return item{
		produto: produto{
			descricao:          p.descricao,
			preco:              p.preco,
			frete:              p.frete,
			percentualDesconto: p.percentualDesconto,
			estoque:            p.estoque,
			imposto:            p.imposto,
		},
		quantidade: p.quantidade,
	}
}
