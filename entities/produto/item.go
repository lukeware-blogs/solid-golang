package produto

// Utilizando OCP e LSP
type item struct {
	produto
	quantidade uint64
}

func (i item) Quantidade() uint64 {
	return i.quantidade
}

func (i item) QuantidadeAtualizada() uint64 {
	return i.estoque.QuantidadeAtualizada()
}

func (p item) ValidarItens() (bool, error) {
	produtoValido, erro := p.ValidarDados()
	estoqueValido, erroEstoque := p.estoque.ValidarDados(p.quantidade)
	switch {
	case !produtoValido:
		return produtoValido, erro
	case !estoqueValido:
		return estoqueValido, erroEstoque
	default:
		return true, nil
	}
}
