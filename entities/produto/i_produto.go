package produto

// Utilizando ISP
type IProduto interface {
	Descricao() string
	Preco() float64
	Frete() float64
	PrecoComDesconto() float64
	ValidarDados() (bool, error)
}
