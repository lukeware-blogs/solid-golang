package produto

import (
	"fmt"

	"labsit.io/solid-golang/entities/estoque"
	"labsit.io/solid-golang/entities/imposto"
)

// Utilizando OCP
type produto struct {
	descricao          string
	preco              float64
	frete              float64
	percentualDesconto float64
	estoque            estoque.IEstoque
	imposto            imposto.IImposto
}

func (p produto) Descricao() string {
	return p.descricao
}

func (p produto) Preco() float64 {
	precoComIcms := p.preco * p.imposto.Icms()
	precoComPis := p.preco * p.imposto.Pis()
	precoComCofins := p.preco * p.imposto.Cofins()
	return p.preco + precoComIcms + precoComPis + precoComCofins
}

func (p produto) Frete() float64 {
	return p.frete
}

func (p produto) PrecoComDesconto() float64 {
	preco := p.Preco()
	desconto := (preco * p.percentualDesconto)
	return preco - desconto
}

func (p produto) ValidarDados() (bool, error) {
	switch {
	case p.frete <= 0:
		return false, fmt.Errorf("Frete não encontrada")
	case p.percentualDesconto < 0:
		return false, fmt.Errorf("Desconto inválido")
	case p.preco <= 0:
		return false, fmt.Errorf("Preço inválido")
	default:
		return true, nil
	}
}
