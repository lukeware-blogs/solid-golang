package produto

// Utilizando ISP
type IItem interface {
	IProduto
	Quantidade() uint64
	QuantidadeAtualizada() uint64
	ValidarItens() (bool, error)
}
