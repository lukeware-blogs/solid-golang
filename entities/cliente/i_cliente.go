package cliente

// Utilizando ISP
type ICliente interface {
	Nome() string
	DocumentoIdentificacao() string
	Telefone() string
	ValidarDados() (bool, error)
}
