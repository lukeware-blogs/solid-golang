package cliente

// Utilizando design pattern builder, Utilizando DIP
type clienteBuilder struct {
	nome                   string
	documentoIdentificacao string
	telefone               string
}

func New() *clienteBuilder {
	return new(clienteBuilder)
}

func (c *clienteBuilder) Nome(nome string) *clienteBuilder {
	c.nome = nome
	return c
}

func (c *clienteBuilder) DocumentoIdentificacao(documentoIdentificacao string) *clienteBuilder {
	c.documentoIdentificacao = documentoIdentificacao
	return c
}

func (c *clienteBuilder) Telefone(telefone string) *clienteBuilder {
	c.telefone = telefone
	return c
}

func (c *clienteBuilder) Build() ICliente {
	return cliente{nome: c.nome, documentoidentificacao: c.documentoIdentificacao, telefone: c.telefone}
}
