package cliente

import "fmt"

// Utilizando OCP
type cliente struct {
	nome                   string
	documentoidentificacao string
	telefone               string
}

func (c cliente) Nome() string {
	return c.nome
}

func (c cliente) DocumentoIdentificacao() string {
	return c.documentoidentificacao
}

func (c cliente) Telefone() string {
	return c.telefone
}

func (c cliente) ValidarDados() (bool, error) {
	switch {
	case c.documentoidentificacao == "":
		return false, fmt.Errorf("Documento de identificação não encontrado")
	case c.nome == "":
		return false, fmt.Errorf("Nome do cliente não encontrado")
	case c.telefone == "":
		return false, fmt.Errorf("Telefone não encontrado")
	default:
		return true, nil
	}
}
