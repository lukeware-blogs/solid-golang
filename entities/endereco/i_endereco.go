package endereco

// Utilizando ISP
type IEndereco interface {
	Rua() string
	Numero() string
	Cidade() string
	Estado() string
	Complemento() string
	ValidarDados() (bool, error)
}
