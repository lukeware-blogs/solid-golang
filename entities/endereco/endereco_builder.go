package endereco

// Utilizando design pattern builder, Utilizando DIP
type enderecoBuilder struct {
	rua         string
	numero      string
	cep         string
	cidade      string
	estado      string
	complemento string
}

func New() *enderecoBuilder {
	return new(enderecoBuilder)
}

func (e *enderecoBuilder) Rua(rua string) *enderecoBuilder {
	e.rua = rua
	return e
}

func (e *enderecoBuilder) Cep(cep string) *enderecoBuilder {
	e.cep = cep
	return e
}

func (e *enderecoBuilder) Numero(numero string) *enderecoBuilder {
	e.numero = numero
	return e
}

func (e *enderecoBuilder) Cidade(cidade string) *enderecoBuilder {
	e.cidade = cidade
	return e
}

func (e *enderecoBuilder) Estado(estado string) *enderecoBuilder {
	e.estado = estado
	return e
}

func (e *enderecoBuilder) Complemento(complemento string) *enderecoBuilder {
	e.complemento = complemento
	return e
}

func (e *enderecoBuilder) Build() IEndereco {
	return endereco{rua: e.rua, numero: e.numero, cep: e.cep, cidade: e.cidade, estado: e.estado, complemento: e.complemento}
}
