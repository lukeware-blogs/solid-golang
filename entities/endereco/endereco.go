package endereco

import "fmt"

// Utilizando OCP
type endereco struct {
	rua         string
	numero      string
	cep         string
	cidade      string
	estado      string
	complemento string
}

func (e endereco) Rua() string {
	return e.rua
}

func (e endereco) Numero() string {
	return e.numero
}

func (e endereco) Cidade() string {
	return e.cidade
}

func (e endereco) Estado() string {
	return e.estado
}

func (e endereco) Complemento() string {
	return e.complemento
}

func (e endereco) ValidarDados() (bool, error) {
	switch {
	case e.cep == "":
		return false, fmt.Errorf("Cep não encontrado")
	case e.cidade == "":
		return false, fmt.Errorf("Cidade não encontrado")
	case e.complemento == "":
		return false, fmt.Errorf("Complemento não encontrado")
	case e.estado == "":
		return false, fmt.Errorf("Estado não encontrado")
	case e.numero == "":
		return false, fmt.Errorf("Numero não encontrado")
	case e.rua == "":
		return false, fmt.Errorf("Rua não encontrado")
	default:
		return true, nil
	}
}
