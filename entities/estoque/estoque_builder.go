package estoque

// Utilizando design pattern builder, Utilizando DIP
type EstoqueBuilder struct {
	estoqueDisponivel    bool
	quantidade           uint64
	quantidadeAtualizada uint64
}

func New() *EstoqueBuilder {
	return new(EstoqueBuilder)
}

func (e *EstoqueBuilder) EstoqueDisponivel(estoqueDisponivel bool) *EstoqueBuilder {
	e.estoqueDisponivel = estoqueDisponivel
	return e
}

func (e *EstoqueBuilder) Quantidade(quantidade uint64) *EstoqueBuilder {
	e.quantidade = quantidade
	return e
}

func (e *EstoqueBuilder) QuantidadeAtualizada(quantidadeAtualizada uint64) *EstoqueBuilder {
	e.quantidadeAtualizada = quantidadeAtualizada
	return e
}

func (e *EstoqueBuilder) Build() IEstoque {
	return estoque{estoqueDisponivel: e.estoqueDisponivel, quantidade: e.quantidade, quantidadeAtualizada: e.quantidadeAtualizada}
}
