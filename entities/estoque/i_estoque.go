package estoque

// Utilizando ISP
type IEstoque interface {
	EstoqueDisponivel() bool
	Quantidade() uint64
	QuantidadeAtualizada() uint64
	ValidarDados(quantidadeItens uint64) (bool, error)
}
