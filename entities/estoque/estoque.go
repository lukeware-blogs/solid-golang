package estoque

import "fmt"

//Utilizando OCP
type estoque struct {
	estoqueDisponivel bool
	quantidade        uint64
	quantidadeAtualizada uint64
}

func (e estoque) EstoqueDisponivel() bool {
	return e.estoqueDisponivel
}

func (e estoque) Quantidade() uint64 {
	return e.quantidade
}

func (e estoque) QuantidadeAtualizada() uint64 {
	return e.quantidadeAtualizada
}

func (e estoque) ValidarDados(quantidadeItens uint64) (bool, error) {
	switch {
	case !e.estoqueDisponivel:
		return false, fmt.Errorf("Estoque não está indisponível")
	case e.quantidade < quantidadeItens:
		return false, fmt.Errorf("Estoque insuficiente")
	default:
		return true, nil
	}
}
