# solid-go
Criando uma aplicação que utiliza OOP e SOLID para salvar um pedido.
O objetivo desse projeto é aplicar conceito SOLID para salvar pedidos de compra de um ecomerce.

Não vamos implementar conexão com banco, serviço rest, apenas a camada entity e use case de clean architecture
## Comando para rodar os testes
go test ./...


## Introdução
Esse pequeno projeto desenvolvemos um serviço que salva pedido de compra
- validar preço
- validar pedido
- validar imposto
- validar cliente
- validar estoque
- validar endereco
- validar produto