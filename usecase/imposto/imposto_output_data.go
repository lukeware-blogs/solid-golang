package imposto

type ImpostoOutputData struct {
	Icms   float64
	Pis    float64
	Cofins float64
}
