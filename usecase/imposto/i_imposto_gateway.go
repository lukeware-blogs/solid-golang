package imposto

// Utilizando ISP
type IImpostoGateway interface {
	FindOne(id string) (ImpostoOutputData, error)
}
