package imposto

import "github.com/stretchr/testify/mock"

// Mocando o serviço que busca os imposto daquele produto
type ImpostoGatewayMock struct {
	mock.Mock
}

func (mock *ImpostoGatewayMock) FindOne(id string) (ImpostoOutputData, error) {
	args := mock.Called(id)
	result := args.Get(0)
	return result.(ImpostoOutputData), args.Error(1)
}
