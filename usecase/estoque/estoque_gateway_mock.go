package estoque

import "github.com/stretchr/testify/mock"

// mocando o serviço que traz o estoque do produto
type EstoqueGatewayMock struct {
	mock.Mock
}

func (e *EstoqueGatewayMock) FindOne(id string) (EstoqueOutputData, error) {
	args := e.Called(id)
	results := args.Get(0)
	return results.(EstoqueOutputData), args.Error(1)
}

func (e *EstoqueGatewayMock) Update(id EstoqueInputData) (string, error) {
	args := e.Called(id)
	results := args.Get(0)
	return results.(string), args.Error(1)
}
