package estoque

// Utilizando ISP
type IEstoqueGateway interface {
	FindOne(id string) (EstoqueOutputData, error)
	Update(estoque EstoqueInputData) (string, error)
}
