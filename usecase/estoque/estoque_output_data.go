package estoque

type EstoqueOutputData struct {
	EstoqueDisponivel  bool
	QuantidadeContabel float64
	QuantidadeFisica   uint64
}
