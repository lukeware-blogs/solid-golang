package cliente

import "github.com/stretchr/testify/mock"

// Mocando o serviço que busca os dados completo do cliente
type ClienteGatewayMock struct {
	mock.Mock
}

func (c *ClienteGatewayMock) FindOne(documentoIdentificacao string) (ClienteOutputData, error) {
	args := c.Called(documentoIdentificacao)
	result := args.Get(0)
	return result.(ClienteOutputData), args.Error(1)
}
