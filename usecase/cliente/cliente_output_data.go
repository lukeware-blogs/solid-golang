package cliente

type ClienteOutputData struct {
	Nome                   string
	DocumentoIdentificacao string
	Telefone               string
	Endereco               struct {
		Rua         string
		Numero      string
		Cep         string
		Cidade      string
		Estado      string
		Complemento string
	}
}
