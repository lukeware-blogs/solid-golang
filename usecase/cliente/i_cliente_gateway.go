package cliente

// Utilizando ISP
type IClienteGateway interface {
	FindOne(documentoIdentificacao string) (ClienteOutputData, error)
}
