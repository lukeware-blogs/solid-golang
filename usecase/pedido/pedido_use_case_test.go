package pedido

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"labsit.io/solid-golang/usecase/cliente"
	"labsit.io/solid-golang/usecase/estoque"
	"labsit.io/solid-golang/usecase/imposto"
	"labsit.io/solid-golang/usecase/produto"
)

func Test_gerar_pedido_de_compra(t *testing.T) {
	assert := assert.New(t)

	// instanciando classe mocadas
	produtoGateway := new(produto.ProdutoGatewayMock)
	impostoGateway := new(imposto.ImpostoGatewayMock)
	pedidoPresenter := new(PedidoPresenterMock)
	estoqueGateway := new(estoque.EstoqueGatewayMock)
	pedidoGateway := new(PedidoGatewayMock)
	clienteGateway := new(cliente.ClienteGatewayMock)

	// instanciando caso de uso do pedido
	var pedidoUseCase IPedidoUseCase = NewPedidoUseCase(pedidoPresenter, produtoGateway, impostoGateway, estoqueGateway, pedidoGateway, clienteGateway)

	// preparação comportamental das classe mocadas
	// preparando comportamento do método FindOne do gateway produto
	produtoGateway.On("FindOne", "#8788").Return(produto.ProdutoOutputData{
		Descricao:          "Geladeira",
		Preco:              3500.99,
		Frete:              167.90,
		PercentualDesconto: 0.1,
	}, nil).On("FindOne", "#8789").Return(produto.ProdutoOutputData{
		Descricao:          "TV 55",
		Preco:              5679.99,
		Frete:              53.99,
		PercentualDesconto: 0.1,
	}, nil)

	// preparando comportamento do método FindOne do gateway cliente
	clienteGateway.On("FindOne", "999.999.999-99").Return(cliente.ClienteOutputData{
		Nome:                   "Diego Morais",
		DocumentoIdentificacao: "999.999.999-99",
		Telefone:               "19 9 9999-9999",
		Endereco: struct {
			Rua         string
			Numero      string
			Cep         string
			Cidade      string
			Estado      string
			Complemento string
		}{
			Rua:         "Rua teste",
			Numero:      "8987-A",
			Cep:         "99999-999",
			Cidade:      "São Paulo",
			Estado:      "SP",
			Complemento: "APT 14",
		},
	}, nil)

	// preparando comportamento do método FindOne do gateway imposto
	impostoGateway.On("FindOne", "#8788").Return(imposto.ImpostoOutputData{
		Icms:   0.05,
		Pis:    0.02,
		Cofins: 0.01,
	}, nil).On("FindOne", "#8789").Return(imposto.ImpostoOutputData{
		Icms:   0.2,
		Pis:    0.1,
		Cofins: 0.05,
	}, nil)

	// preparando comportamento do método FindOne do gateway estoque
	estoqueGateway.On("FindOne", "#8788").Return(estoque.EstoqueOutputData{
		EstoqueDisponivel:  true,
		QuantidadeContabel: 10,
		QuantidadeFisica:   10,
	}, nil).On("FindOne", "#8789").Return(estoque.EstoqueOutputData{
		EstoqueDisponivel:  true,
		QuantidadeContabel: 10,
		QuantidadeFisica:   10,
	}, nil)

	// preparando comportamento do método update do gateway estoque
	estoqueGateway.On("Update", mock.Anything).Return("#8789", nil)

	// preparando comportamento do método Salvar do gateway pedido
	pedidoGateway.On("Salvar", mock.Anything).Return("#77788", nil)

	// preparando comportamento do método Success do presenter pedido
	pedidoPresenter.On("Success", "#77788").Return(PedidoResponse{
		Id:       "#77788",
		Mensagem: "Pedido #77788 registrado",
	}, nil)

	// dados que vão chegar pela requisição
	carrinhocompra := CarrinhoCompraRequest{
		Itens: []ItemRequest{
			{Id: "#8788", Quantidade: 1},
			{Id: "#8789", Quantidade: 2},
		},
		ClienteId: "999.999.999-99",
	}

	// Salvar o carrinho de compra
	response := pedidoUseCase.Salvar(carrinhocompra)

	// Validando o resultando
	assert.NotNil(response)
	assert.Equal(response.Id, "#77788")
	assert.Equal(response.Mensagem, "Pedido #77788 registrado")

	produtoGateway.AssertExpectations(t)
	impostoGateway.AssertExpectations(t)
	pedidoPresenter.AssertExpectations(t)
	estoqueGateway.AssertExpectations(t)
	pedidoGateway.AssertExpectations(t)
	clienteGateway.AssertExpectations(t)

	DestroyPedidoUseCase()
}

func Test_gerar_pedido_de_compra_com_produto_sem_estoque(t *testing.T) {
	assert := assert.New(t)

	// instanciando classe mocadas
	produtoGateway := new(produto.ProdutoGatewayMock)
	impostoGateway := new(imposto.ImpostoGatewayMock)
	pedidoPresenter := new(PedidoPresenterMock)
	estoqueGateway := new(estoque.EstoqueGatewayMock)
	pedidoGateway := new(PedidoGatewayMock)
	clienteGateway := new(cliente.ClienteGatewayMock)

	// instanciando caso de uso do pedido
	var pedidoUseCase IPedidoUseCase = NewPedidoUseCase(pedidoPresenter, produtoGateway, impostoGateway, estoqueGateway, pedidoGateway, clienteGateway)

	// preparação comportamental das classe mocadas
	// preparando comportamento do método FindOne do gateway produto
	produtoGateway.On("FindOne", "#8788").Return(produto.ProdutoOutputData{
		Descricao:          "Geladeira",
		Preco:              3500.99,
		Frete:              167.90,
		PercentualDesconto: 0.1,
	}, nil).On("FindOne", "#8789").Return(produto.ProdutoOutputData{
		Descricao:          "TV 55",
		Preco:              5679.99,
		Frete:              53.99,
		PercentualDesconto: 0.1,
	}, nil)

	// preparando comportamento do método FindOne do gateway cliente
	clienteGateway.On("FindOne", "999.999.999-99").Return(cliente.ClienteOutputData{
		Nome:                   "Diego Morais",
		DocumentoIdentificacao: "999.999.999-99",
		Telefone:               "19 9 9999-9999",
		Endereco: struct {
			Rua         string
			Numero      string
			Cep         string
			Cidade      string
			Estado      string
			Complemento string
		}{
			Rua:         "Rua teste",
			Numero:      "8987-A",
			Cep:         "99999-999",
			Cidade:      "São Paulo",
			Estado:      "SP",
			Complemento: "APT 14",
		},
	}, nil)

	// preparando comportamento do método FindOne do gateway imposto
	impostoGateway.On("FindOne", "#8788").Return(imposto.ImpostoOutputData{
		Icms:   0.05,
		Pis:    0.02,
		Cofins: 0.01,
	}, nil).On("FindOne", "#8789").Return(imposto.ImpostoOutputData{
		Icms:   0.2,
		Pis:    0.1,
		Cofins: 0.05,
	}, nil)

	// preparando comportamento do método FindOne do gateway estoque
	estoqueGateway.On("FindOne", "#8788").Return(estoque.EstoqueOutputData{
		EstoqueDisponivel:  false,
		QuantidadeContabel: 1,
		QuantidadeFisica:   0,
	}, nil).On("FindOne", "#8789").Return(estoque.EstoqueOutputData{
		EstoqueDisponivel:  false,
		QuantidadeContabel: 1,
		QuantidadeFisica:   0,
	}, nil)

	// preparando comportamento do método Success do presenter pedido
	pedidoPresenter.On("Fail", "Estoque não está indisponível").Return(PedidoResponse{
		Id:       "",
		Mensagem: "Estoque não está indisponível",
	}, nil)

	// dados que vão chegar pela requisição
	carrinhocompra := CarrinhoCompraRequest{
		Itens: []ItemRequest{
			{Id: "#8788", Quantidade: 1},
			{Id: "#8789", Quantidade: 2},
		},
		ClienteId: "999.999.999-99",
	}

	// Salvar o carrinho de compra
	response := pedidoUseCase.Salvar(carrinhocompra)

	// Validando o resultando
	assert.NotNil(response)
	assert.Equal(response.Id, "")
	assert.Equal(response.Mensagem, "Estoque não está indisponível")

	produtoGateway.AssertExpectations(t)
	impostoGateway.AssertExpectations(t)
	estoqueGateway.AssertExpectations(t)
	clienteGateway.AssertExpectations(t)

	DestroyPedidoUseCase()
}
