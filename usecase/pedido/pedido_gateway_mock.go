package pedido

import (
	"github.com/stretchr/testify/mock"
	"labsit.io/solid-golang/entities/pedido"
)

// Mocando o serviço que salva pedido de compra
type PedidoGatewayMock struct {
	mock.Mock
}

func (p *PedidoGatewayMock) Salvar(pedidoCompra pedido.IPedido) (string, error) {
	args := p.Called(pedidoCompra)
	result := args.Get(0)
	return result.(string), args.Error(1)
}
