package pedido

import "labsit.io/solid-golang/entities/pedido"

// Utilizando ISP
type IPedidoGateway interface {
	Salvar(pedidoCompra pedido.IPedido) (string, error)
}
