package pedido

import (
	"sync"

	"labsit.io/solid-golang/usecase/cliente"
	"labsit.io/solid-golang/usecase/estoque"
	"labsit.io/solid-golang/usecase/imposto"
	"labsit.io/solid-golang/usecase/produto"
)

var (
	pedidoUsecase *pedidoUseCase = nil
	mu            sync.Mutex
)

// Utilizando design pattern Singleton
func NewPedidoUseCase(
	pedidoPresenter IPedidoPresenter,
	produtoGateway produto.IProdutoGateway,
	impostoGateway imposto.IImpostoGateway,
	estoqueGateway estoque.IEstoqueGateway,
	pedidoGateway IPedidoGateway,
	clienteGateway cliente.IClienteGateway,
) IPedidoUseCase {
	if pedidoUsecase == nil {
		mu.Lock()
		if pedidoUsecase == nil {
			pedidoUsecase = &pedidoUseCase{
				pedidoPresenter: pedidoPresenter,
				produtoGateway:  produtoGateway,
				impostoGateway:  impostoGateway,
				estoqueGateway:  estoqueGateway,
				pedidoGateway:   pedidoGateway,
				clienteGateWay:  clienteGateway,
			}
		}
		mu.Unlock()
	}
	return pedidoUsecase
}

func DestroyPedidoUseCase() {
	if pedidoUsecase != nil {
		mu.Lock()
		if pedidoUsecase != nil {
			pedidoUsecase = nil
		}
		mu.Unlock()
	}
}
