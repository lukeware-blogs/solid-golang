package pedido

// Utilizando ISP
type IPedidoPresenter interface {
	Success(id string) PedidoResponse
	Fail(fail string) PedidoResponse
}
