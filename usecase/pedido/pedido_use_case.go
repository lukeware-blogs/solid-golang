package pedido

import (
	clienteEntity "labsit.io/solid-golang/entities/cliente"
	"labsit.io/solid-golang/entities/endereco"
	estadoEntity "labsit.io/solid-golang/entities/estoque"
	impostoUsecase "labsit.io/solid-golang/entities/imposto"
	"labsit.io/solid-golang/entities/pedido"
	"labsit.io/solid-golang/entities/produto"
	"labsit.io/solid-golang/usecase/cliente"
	"labsit.io/solid-golang/usecase/estoque"
	"labsit.io/solid-golang/usecase/imposto"
	produtoUsecase "labsit.io/solid-golang/usecase/produto"
)

// Utilizando SRP, OCP, DIP, e ISP
type pedidoUseCase struct {
	pedidoPresenter IPedidoPresenter
	pedidoGateway   IPedidoGateway
	produtoGateway  produtoUsecase.IProdutoGateway
	impostoGateway  imposto.IImpostoGateway
	estoqueGateway  estoque.IEstoqueGateway
	clienteGateWay  cliente.IClienteGateway
}

func (p pedidoUseCase) Salvar(request CarrinhoCompraRequest) PedidoResponse {
	clienteData, _ := p.clienteGateWay.FindOne(request.ClienteId)

	cliente := clienteEntity.New().DocumentoIdentificacao(clienteData.DocumentoIdentificacao).Nome(clienteData.Nome).Telefone(clienteData.Telefone).Build()
	itens := p.getItens(request)
	endereco := endereco.New().Cep(clienteData.Endereco.Cep).Rua(clienteData.Endereco.Rua).Cidade(clienteData.Endereco.Cidade).Estado(clienteData.Endereco.Estado).Complemento(clienteData.Endereco.Complemento).Numero(clienteData.Endereco.Numero).Build()

	carrinhoCompra, err := p.getCarrinhoDeCompra(cliente, endereco, itens)
	if err != nil {
		return p.pedidoPresenter.Fail(err.Error())
	}
	// implementando LSP
	id, _ := p.pedidoGateway.Salvar(carrinhoCompra)
	p.atualizarEstoque(itens)

	return p.pedidoPresenter.Success(id)
}

func (p pedidoUseCase) getItens(request CarrinhoCompraRequest) map[string]produto.IItem {
	itens := make(map[string]produto.IItem)

	for _, itemRequest := range request.Itens {
		produtoOutput, _ := p.produtoGateway.FindOne(itemRequest.Id)
		estoqueOutput, _ := p.estoqueGateway.FindOne(itemRequest.Id)
		impostoOutput, _ := p.impostoGateway.FindOne(itemRequest.Id)

		quantidadeAtualizado := estoqueOutput.QuantidadeFisica - itemRequest.Quantidade
		imposto := impostoUsecase.New().Pis(impostoOutput.Pis).Icms(impostoOutput.Cofins).Icms(impostoOutput.Icms).Build()
		estoque := estadoEntity.New().EstoqueDisponivel(estoqueOutput.EstoqueDisponivel).Quantidade(uint64(estoqueOutput.QuantidadeFisica)).QuantidadeAtualizada(quantidadeAtualizado).Build()

		item := produto.New().Imposto(imposto).Estoque(estoque).Descricao(produtoOutput.Descricao).Frete(produtoOutput.Frete).PercentualDesconto(produtoOutput.PercentualDesconto).Preco(produtoOutput.Preco).Quantidade(itemRequest.Quantidade).Build()
		itens[itemRequest.Id] = item
	}
	return itens
}

func (p pedidoUseCase) getCarrinhoDeCompra(cliente clienteEntity.ICliente, endereco endereco.IEndereco, itens map[string]produto.IItem) (pedido.ICarrinhoCompra, error) {
	listaItens := make([]produto.IItem, 0)

	for _, item := range itens {
		listaItens = append(listaItens, item)
	}

	var carrinhoCompra pedido.ICarrinhoCompra = pedido.New().Cliente(cliente).EnderecoEntrega(endereco).Itens(listaItens).Build()
	_, error := carrinhoCompra.ValidarDados()
	if error != nil {
		return nil, error
	}
	return carrinhoCompra, nil
}

func (p pedidoUseCase) atualizarEstoque(itens map[string]produto.IItem) {
	for id, item := range itens {
		p.estoqueGateway.Update(estoque.EstoqueInputData{
			ID:         id,
			Quantidade: item.QuantidadeAtualizada(),
		})
	}
}
