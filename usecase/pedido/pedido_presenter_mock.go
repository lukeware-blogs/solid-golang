package pedido

import (
	"github.com/stretchr/testify/mock"
)

// Mocando o serviço de apresentação após salvar
type PedidoPresenterMock struct {
	mock.Mock
}

func (p *PedidoPresenterMock) Success(id string) PedidoResponse {
	args := p.Called(id)
	result := args.Get(0)
	return result.(PedidoResponse)
}

func (p *PedidoPresenterMock) Fail(fail string) PedidoResponse {
	args := p.Called(fail)
	result := args.Get(0)
	return result.(PedidoResponse)
}
