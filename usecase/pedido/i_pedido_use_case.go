package pedido

// Utilizando ISP
type IPedidoUseCase interface {
	Salvar(carrinhoCompra CarrinhoCompraRequest) PedidoResponse
}
