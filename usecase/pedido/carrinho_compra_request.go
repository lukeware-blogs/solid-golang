package pedido

type CarrinhoCompraRequest struct {
	Itens     []ItemRequest
	ClienteId string
}
