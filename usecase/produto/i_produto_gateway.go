package produto

type IProdutoGateway interface {
	FindOne(id string) (ProdutoOutputData, error)
}
