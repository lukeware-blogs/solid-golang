package produto

import (
	"github.com/stretchr/testify/mock"
)

type ProdutoGatewayMock struct {
	mock.Mock
}

func (mock *ProdutoGatewayMock) FindOne(id string) (ProdutoOutputData, error) {
	args := mock.Called(id)
	result := args.Get(0)
	return result.(ProdutoOutputData), args.Error(1)
}
