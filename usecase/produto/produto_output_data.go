package produto

type ProdutoOutputData struct {
	Descricao          string
	PercentualDesconto float64
	Preco              float64
	Frete              float64
}
